import App, { Container } from "next/app";
import Head from "next/head";
import React from "react";
import "../App.css";
import { Layout, Menu } from "antd";
import Link from "next/link";
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined
} from "@ant-design/icons";


export default class RootApp extends App {
  render() {
    const { Component, ...other } = this.props;
    const { Header, Footer, Sider, Content } = Layout;

    return (
      <Layout>
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={broken => {
            console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              <Link href="/senior/senior">어르신</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              <Link href="/page2">출석차량</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
            <Menu.Item key="4" icon={<UserOutlined />}>
              nav 4
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{ padding: 0 }}
          >
          </Header>
          <Content style={{ margin: '6px 0', overflow: 'auto' }}>
            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              <Component {...other} />
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            예스케어시스템즈 ©2021 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    );
  }
}
