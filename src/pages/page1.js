import {PageHeader} from "antd";

function Page1() {
  return (
    <PageHeader
      className="site-page-header"
      onBack={() => null}
      title="Title"
      subTitle="This is a subtitle"
    />
  );
}

export default Page1;
