import React, { useState } from "react";
import {
  Button,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  PageHeader,
  Row,
  Table,
  Tag
} from "antd";
import { DownOutlined, UpOutlined } from "@ant-design/icons";

const RecordMonthPlan = () => {
  const columns = [
    {
      title: "날짜",
      dataIndex: "date",
      render: text => <a>{text}</a>
    },
    {
      title: "공단통보인원",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    },
    {
      title: "배차인원",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    },
    {
      title: "등원",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    },
    {
      title: "하원",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    },
    {
      title: "실제출석",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    },
    {
      title: "미이용인원",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    },
    {
      title: "청구인원",
      className: "column-money",
      dataIndex: "money",
      align: "right"
    }
  ];

  const data = [
    {
      key: "1",
      name: "John Brown",
      money: "62명",
      address: "New York No. 1 Lake Park",
      date: "2020-02-01 월"
    },
    {
      key: "2",
      name: "Jim Green",
      money: "58명",
      date: "2020-02-02 화"
    },
    {
      key: "3",
      name: "Joe Black",
      money: "59명",
      date: "2020-02-03 수"
    },
    {
      key: "4",
      name: "Joe Black",
      money: "59명",
      date: "2020-02-04 목"
    },
    {
      key: "5",
      name: "Joe Black",
      money: "59명",
      date: "2020-02-05 금"
    }
  ];

  const Content = ({ children, extraContent }) => (
    <Row>
      <div style={{ flex: 1 }}>{children}</div>
      <div className="image">{extraContent}</div>
    </Row>
  );

  const onFinish = (values: any) => {
    console.log("Received values of form: ", values);
  };

  const [expand, setExpand] = useState(false);
  const [form] = Form.useForm();

  const getFields = () => {
    const count = expand ? 10 : 1;

    const children = [];
    for (let i = 0; i < count; i++) {
      children.push(
        <Col span={8} key={i}>
          <Form.Item
            name={`field-${i}`}
            label={`Field ${i}`}
            rules={[
              {
                required: true,
                message: "Input something!"
              }
            ]}
          >
            <Input placeholder="placeholder" />
          </Form.Item>
        </Col>
      );
    }
    return children;
  };

  function onChange(date, dateString) {
    console.log(date, dateString);
  }

  return (
    <>
      <PageHeader
        title="출석차량 리스트"
        className="site-page-header"
        subTitle="이번달 출석 차량 리스트입니다. 일별 운행정보는 날짜를 클릭해서 확인해 주세요."
        tags={<Tag color="blue">Running</Tag>}
        extra={[
          <Button key="3">Operation</Button>,
          <Button key="2">Operation</Button>,
          <Button key="1" type="primary">
            Primary
          </Button>
        ]}
      >
        <Form
          form={form}
          name="advanced_search"
          className="ant-advanced-search-form"
          onFinish={onFinish}
        >
          <Row>
            <Col span={16}>
              <Form.Item
                name="date"
                label="조회연월"
                rules={[
                  {
                    required: true,
                    message: "Input something!"
                  }
                ]}
              >
                <DatePicker onChange={onChange} picker="month" />
              </Form.Item>
            </Col>
            <Col span={8} style={{ textAlign: "right" }}>
              <Button type="primary" htmlType="submit">
                Search
              </Button>
              <Button
                style={{ margin: "0 8px" }}
                onClick={() => {
                  form.resetFields();
                }}
              >
                Clear
              </Button>
              <a
                style={{ fontSize: 12 }}
                onClick={() => {
                  setExpand(!expand);
                }}
              >
                {expand ? <UpOutlined /> : <DownOutlined />} Collapse
              </a>
            </Col>
          </Row>
        </Form>

        <Divider />
        <Table
          columns={columns}
          dataSource={data}
          bordered
          pagination={false}
          title={() => "2021년 2월 1째주"}
          size="small"
        />
        <Divider />
        <Table
          columns={columns}
          dataSource={data}
          bordered
          title={() => "2021년 2월 2째주"}
        />
        <Table
          columns={columns}
          dataSource={data}
          bordered
          title={() => "2021년 2월 3째주"}
        />
        <Table
          columns={columns}
          dataSource={data}
          bordered
          title={() => "2021년 2월 4째주"}
        />
      </PageHeader>
    </>
  );
};

export default RecordMonthPlan;
