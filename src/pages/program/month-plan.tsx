import {
  Calendar,
  Badge,
  Tag,
  Button,
  PageHeader,
  Table,
  Space,
  Divider
} from "antd";
import React from "react";

function getListData(value) {
  let listData;
  switch (value.date()) {
    case 8:
      listData = [
        { type: "warning", content: "게이트볼 & 보드게임" },
        { type: "success", content: "종이접기" }
      ];
      break;
    case 10:
      listData = [
        { type: "warning", content: "This is warning event." },
        { type: "success", content: "This is usual event." },
        { type: "error", content: "This is error event." }
      ];
      break;
    case 15:
      listData = [
        { type: "warning", content: "This is warning event" },
        { type: "success", content: "This is very long usual event。。...." },
        { type: "error", content: "This is error event 1." },
        { type: "error", content: "This is error event 2." },
        { type: "error", content: "This is error event 3." },
        { type: "error", content: "This is error event 4." }
      ];
      break;
    default:
  }
  return listData || [];
}

const columns = [
  {
    title: "주기",
    dataIndex: "name",
    key: "name",
    align: "center",
    render: text => <a>{text}</a>
  },
  {
    title: "신체기능향상",
    dataIndex: "age",
    key: "age",
    align: "center"
  },
  {
    title: "인지기능향상",
    dataIndex: "address",
    key: "address",
    align: "center"
  },
  {
    title: "인지활동형",
    key: "tags",
    dataIndex: "program4",
    align: "center"
  },
  {
    title: "맞춤형",
    key: "action",
    dataIndex: "program5",
    align: "center"
  }
];

const data = [
  {
    key: "1",
    name: "1주차",
    age: 3,
    address: "2",
    program4: "2",
    program5: "4",
    tags: ["nice", "developer"]
  },
  {
    key: "2",
    name: "2주차",
    age: 4,
    address: "5",
    program4: "2",
    program5: "4",
    tags: ["loser"]
  },
  {
    key: "3",
    name: "3주차",
    age: 4,
    address: "2",
    program4: "2",
    program5: "4",
    tags: ["cool", "teacher"]
  },
  {
    key: "4",
    name: "4주차",
    age: 3,
    address: "6",
    program4: "2",
    program5: "3",
    tags: ["cool", "teacher"]
  }
];

function dateCellRender(value) {
  const listData = getListData(value);
  return (
    <ul className="events">
      {listData.map(item => (
        <li key={item.content}>
          <Badge status={item.type} text={item.content} />
        </li>
      ))}
    </ul>
  );
}

function getMonthData(value) {
  if (value.month() === 8) {
    return 1394;
  }
}

function monthCellRender(value) {
  const num = getMonthData(value);
  return num ? (
    <div className="notes-month">
      <section>{num}</section>
      <span>Backlog number</span>
    </div>
  ) : null;
}

export default function ProgramMonthPlan() {
  return (
    <PageHeader
      title="프로그램 리스트"
      className="site-page-header"
      subTitle="프로그램 월별 일정 리스트입니다."
      tags={<Tag color="blue">Running</Tag>}
      extra={[
        <Button key="3">Operation</Button>,
        <Button key="2">Operation</Button>,
        <Button key="1" type="primary">
          Primary
        </Button>
      ]}
    >
      <Calendar
        dateCellRender={dateCellRender}
        monthCellRender={monthCellRender}
      />
      <Divider />
      <Table columns={columns} dataSource={data} />
    </PageHeader>
  );
}
