import App, { Container } from "next/app";
import Head from "next/head";
import React from "react";
import "../App.css";
import { Layout, Menu } from "antd";
import Link from "next/link";
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined
} from "@ant-design/icons";
// (and @fullcalendar/interaction has no stylesheet)

export default class RootApp extends App {
  render() {
    const { Component, ...other } = this.props;
    const { Header, Footer, Sider, Content } = Layout;

    return (
      <Layout>
        <Layout>
          <Header
            style={{ background: "#fff", textAlign: "center", padding: 0 }}
          >
            <div className="logo" />
            <Menu mode="horizontal" defaultSelectedKeys={["1"]}>
              <Menu.Item key="1" icon={<UserOutlined />}>
                <Link href="/counsel/senior">어르신</Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                <Link href="/car/month-plan">출석차량</Link>
              </Menu.Item>
              <Menu.Item key="3" icon={<UploadOutlined />}>
                <Link href="/program/month-plan">프로그램</Link>
              </Menu.Item>
              <Menu.Item key="4" icon={<UserOutlined />}>
                <Link href="/record/month-plan">제공기록</Link>
              </Menu.Item>
            </Menu>
          </Header>
          <Content style={{ margin: "6px 0", overflow: "auto" }}>
            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              <Component {...other} />
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            예스케어시스템즈 ©2021 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    );
  }
}
