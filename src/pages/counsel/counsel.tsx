import React from "react";
import { Button, message, Steps } from "antd";
import CounselWorkPlan from "./counsel-work-plan";

const { Step } = Steps;

const steps = [
  {
    title: "표준장기요양계획서",
    content: "표준장기요양계획서"
  },
  {
    title: "위험도평가",
    content: "위험도평가"
  },
  {
    title: "욕구평가",
    content: "욕구평가"
  }
];

const Counsel = () => {
  const [current, setCurrent] = React.useState(0);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  return (
    <>
      <Steps current={current}>
        {steps.map(item => (
          <Step key={item.title} title={item.title} />
        ))}
      </Steps>
      <div className="steps-content">
        <CounselWorkPlan />
      </div>
      <div className="steps-action">
        {current < steps.length - 1 && (
          <Button type="primary" onClick={() => next()}>
            Next
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button
            type="primary"
            onClick={() => message.success("Processing complete!")}
          >
            Done
          </Button>
        )}
        {current > 0 && (
          <Button style={{ margin: "0 8px" }} onClick={() => prev()}>
            Previous
          </Button>
        )}
      </div>
    </>
  );
};

export default Counsel;
