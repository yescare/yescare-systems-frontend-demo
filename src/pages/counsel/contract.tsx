import React, { useState } from "react";
import {
  Button,
  DatePicker,
  Descriptions,
  Form,
  Input,
  InputNumber,
  Modal,
  Radio,
  Space
} from "antd";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 }
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not a valid email!",
    number: "${label} is not a valid number!"
  },
  number: {
    range: "${label} must be between ${min} and ${max}"
  }
};

const Step2 = () => {
  function onChange(date, dateString) {
    console.log(date, dateString);
  }

  return (
    <Space direction="vertical">
      <DatePicker onChange={onChange} />
    </Space>
  );
};

const ContractForm = () => {
  const [radioValue, setRadioValue] = useState(0);
  const onFinish = (values: any) => {
    console.log(values);
  };

  const onChange = e => {
    setRadioValue(e.target.value);
    return <Step2 />;
  };

  const radioStyle = {
    display: "block",
    height: "30px",
    lineHeight: "30px"
  };

  return (
    <>
      <Descriptions
        style={{ marginBottom: 16 }}
        title="신규 계약을 위해 해당하는 항목을 선택해 주세요."
      />
      <Radio.Group onChange={onChange} value={radioValue}>
        <Radio style={radioStyle} value={1}>
          표준장기이용계획서
        </Radio>
        <Radio style={radioStyle} value={2}>
          장기요양인정번호 / 인정 유효기간
        </Radio>
        <Radio style={radioStyle} value={3}>
          등급외 어르신
        </Radio>
      </Radio.Group>
    </>
  );
};

const ShowModal = () => {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const cancelModal = () => {
    setVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Open Modal with customized footer
      </Button>
      <Modal
        visible={visible}
        title="계약(신규)"
        closable={visible}
        onCancel={cancelModal}
        footer={[
          <Button key="back" onClick={cancelModal}>
            취소
          </Button>,
          <Button key="submit" type="primary" loading={loading}>
            다음
          </Button>
        ]}
      >
        <ContractForm />
      </Modal>
    </>
  );
};

ShowModal.defaultProps = {};

export default ShowModal;
