import React from "react";
import { Row, Col, Card, Empty } from "antd";

const style = { background: "#0092ff", padding: "8px 0" };

const CounselWorkPlan = () => {
  return (
    <Row gutter={16}>
      <Col className="gutter-row" span={18}>
        <Card title="Card title" bordered={false}>
          <Empty />
        </Card>
      </Col>
      <Col className="gutter-row" span={6}>
        <Card title="Card title" bordered={false}>
          <Empty />
        </Card>
      </Col>
    </Row>
  );
};

export default CounselWorkPlan;
