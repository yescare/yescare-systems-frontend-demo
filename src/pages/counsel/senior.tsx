import React, { useState } from "react";
import {
  Button,
  Typography,
  Divider,
  List,
  Modal,
  Table,
  Space,
  Tag,
  Collapse,
  Card,
  Radio,
  Form,
  Input,
  Col,
  Drawer,
  Row
} from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";

const data1 = [
  "Racing car sprays burning fuel into crowd.",
  "Japanese princess to wed commoner.",
  "Australian walks 100km after outback crash.",
  "Man charged over missing wedding girl.",
  "Los Angeles battles huge wildfires."
];
const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    render: text => <a>{text}</a>
  },
  {
    title: "1월",
    key: "age",
    dataIndex: "tags",
    render: tags => (
      <>
        {tags.map(tag => {
          let color = "geekblue";
          let isDone = "완료";
          if (tag === "N") {
            color = "volcano";
            isDone = "미완료";
          }
          if (tag === "Y") {
            color = "green";
          }
          return (
            <Tag color={color} key={tag}>
              {isDone}
            </Tag>
          );
        })}
      </>
    )
  },
  {
    title: "2월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "3월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "4월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "5월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "6월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "7월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "8월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "9월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "10월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "11월",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "12월",
    dataIndex: "age",
    key: "age"
  }
];

const data = [
  {
    key: "1",
    name: "계약(신규)",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["N"],
    href: "/contract"
  },
  {
    key: "2",
    name: "표준장기이용계획서",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["N"],
    href: "/counsel-work-plan"
  },
  {
    key: "3",
    name: "위험도평가",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["Y"]
  },
  {
    key: "4",
    name: "욕구평가",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["Y"]
  },
  {
    key: "5",
    name: "상담일지",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["N"]
  },
  {
    key: "6",
    name: "제공계획서",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["N"]
  },
  {
    key: "7",
    name: "제공계획서 모니터링",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["N"]
  },
  {
    key: "8",
    name: "관리자 업무수행일지",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["N"]
  },
  {
    key: "9",
    name: "급여제공기록지",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["N"]
  },
  {
    key: "10",
    name: "본인부담금비율",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["Y"]
  }
];
const { Panel } = Collapse;

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const dataSource2 = [
  {
    key: "1",
    name: "Mike",
    age: 32,
    address: "10 Downing Street"
  },
  {
    key: "2",
    name: "John",
    age: 42,
    address: "10 Downing Street"
  }
];

const columns2 = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age"
  }
];

type RequiredMark = boolean | "optional";

const SeniorPage = () => {
  const [state, setState] = useState({ visible: false, placement: "right" });
  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  const onCloseDrawer = prevState => ({
    ...prevState,
    visible: false
  });

  const onClickDrawer = () => {
    setState({ visible: true, placement: "right" });
  };
  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 }
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 }
  };

  /* space에 display: inline-flex 로 되어있어서 수정함 */

  return (
    <>
      <Space direction="vertical" style={{ display: "contents" }}>
        <Collapse ghost collapsible="header">
          <Panel header="김윤진 (등급없음/1989-09-09)" key="1">
            <Row>
              <Col span={12}>
                <Divider orientation="left">어르신 기초정보</Divider>
                <Form
                  {...layout}
                  name="basic"
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                >
                  <Form.Item
                    label="성명"
                    required
                    tooltip="This is a required field"
                  >
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                  <Form.Item
                    label="주민번호"
                    tooltip={{
                      title: "Tooltip with customize icon",
                      icon: <InfoCircleOutlined />
                    }}
                  >
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                  <Form.Item
                    label="실제생신"
                    tooltip={{
                      title: "Tooltip with customize icon",
                      icon: <InfoCircleOutlined />
                    }}
                  >
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                  <Form.Item
                    label="주소"
                    tooltip={{
                      title: "Tooltip with customize icon",
                      icon: <InfoCircleOutlined />
                    }}
                  >
                    <Input placeholder="input placeholder" />
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                </Form>
                <Divider orientation="left">보호자 정보</Divider>
                <Form
                  {...layout}
                  name="basic"
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                >
                  <Form.Item
                    label="성명"
                    required
                    tooltip="This is a required field"
                  >
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                  <Form.Item
                    label="주민번호"
                    tooltip={{
                      title: "Tooltip with customize icon",
                      icon: <InfoCircleOutlined />
                    }}
                  >
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                  <Form.Item
                    label="실제생신"
                    tooltip={{
                      title: "Tooltip with customize icon",
                      icon: <InfoCircleOutlined />
                    }}
                  >
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                  <Form.Item
                    label="주소"
                    tooltip={{
                      title: "Tooltip with customize icon",
                      icon: <InfoCircleOutlined />
                    }}
                  >
                    <Input placeholder="input placeholder" />
                    <Input placeholder="input placeholder" />
                  </Form.Item>
                </Form>
                <Divider orientation="left">기타 정보</Divider>
              </Col>
              <Col span={12}>
                <></>
              </Col>
            </Row>
          </Panel>
        </Collapse>
      </Space>

      <Divider orientation="left">급여등록정보</Divider>
      <Table columns={columns} dataSource={data} />

      <div className="site-drawer-render-in-current-wrapper">
        <div style={{ marginTop: 16 }}>
          <Button type="primary" onClick={onClickDrawer}>
            Open
          </Button>
        </div>
        <Drawer
          title="Basic Drawer"
          placement="right"
          closable={false}
          onClose={e => setState({ ...state, visible: false })}
          visible={state.visible}
          getContainer={false}
          style={{ position: "absolute" }}
        >
          <Table dataSource={dataSource2} columns={columns2} />
        </Drawer>
      </div>
    </>
  );
};

export default SeniorPage;
